import debounce from 'lodash.debounce';
import { ICreditFormData, IInitialValues } from '../components/Form/types/form.types';

export const initialSelectorsData: ICreditFormData = {
  activityType: [],
  creditType: [],
  creditAmount: [],
};

class CreditFormApi {
  constructor(private apiUrl: string) {}

  async submitForm(values: Required<IInitialValues>) {
    const parsedDate = new Date(values.dateCompleted!).toISOString().slice(0, 10);

    try {
      await fetch(`${this.apiUrl}/submit_form`, {
        body: JSON.stringify({ ...values, dateCompleted: parsedDate }),
        method: 'POST',
      });
    } catch (error) {
      // TODO remove this after move to prod
      return 'Success';
    }
  }

  validateEmail(email: string | undefined): Promise<boolean> {
    return new Promise(res => {
      setTimeout(() => {
        res(email === 'a@a');
      }, 2000);
    });
  }

  async getDateForCreditForm(): Promise<ICreditFormData> {
    try {
      const response = await fetch('form_data.json');
      return await response.json();
    } catch (error) {
      return initialSelectorsData;
    }
  }
}

const apiUrl = import.meta.env.VITE_API_URL;
export const creditFormApi = new CreditFormApi(apiUrl);

export const debounced = debounce(creditFormApi.validateEmail, 300);

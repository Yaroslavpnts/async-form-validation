export interface IInitialValues {
  email: string;
  activityType: string;
  dateCompleted: Date | null;
  creditType: string;
  creditAmount: string;
  description: string;
  reflection: string;
}

export interface ICreditFormData {
  activityType: string[];
  creditType: string[];
  creditAmount: string[];
}

import * as Yup from 'yup';
import { creditFormApi } from '../../../api/submitForm';

export const validationSchema = Yup.object({
  email: Yup.string()
    .test('checkEmailUnique', 'This email is already registered.', async (value, context) => {
      try {
        await Yup.string().required('Required').email('Invalid email').validate(value);
        const isEmailUnique = await creditFormApi.validateEmail(value);

        if (isEmailUnique === false) {
          return false;
        }

        return true;
      } catch (error) {
        if (error instanceof Yup.ValidationError) {
          return context.createError({ message: error.message });
        }
        return context.createError({ message: 'Unexpected error' });
      }
    })
    .required('Required'),
  activityType: Yup.string().required('Required'),
  dateCompleted: Yup.string().required('Required'),
  creditType: Yup.string().required('Required'),
  creditAmount: Yup.string().required('Required'),
  description: Yup.string().max(1000, 'Max length 1000 symbols'),
  reflection: Yup.string().max(1000, 'Max length 1000 symbols'),
});

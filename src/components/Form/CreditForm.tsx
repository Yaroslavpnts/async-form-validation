import { useEffect, useState, useMemo } from 'react';
import { useFormik } from 'formik';
import cn from 'classnames';
import DatePicker from 'react-datepicker';
import debounce from 'lodash.debounce';
import { IInitialValues } from './types/form.types';
import { InputField } from './components/InputField/InputField';
import { MySelect } from './components/Select/Select';
import { SuccessfulSubmit } from './components/SuccessfulSubmit/SuccessfulSubmit';
import { validationSchema } from './validation/validationSchema';
import { creditFormApi, initialSelectorsData } from '../../api/submitForm';
import 'react-datepicker/dist/react-datepicker.css';
import './index.css';

const initialFormData: IInitialValues = {
  email: '',
  activityType: '',
  dateCompleted: null,
  creditType: '',
  creditAmount: '',
  description: '',
  reflection: '',
};

const maxDate = new Date();
maxDate.setDate(maxDate.getDate() - 1);

const formColumnStyles = 'w-full md:w-1/2 py-10 md:py-20 px-8 sm:px-20 md:px-14';

export const CreditForm: React.FC = () => {
  const [selectData, setSelectData] = useState(initialSelectorsData);
  const [successfulSubmit, setSuccessfulSubmit] = useState(false);

  useEffect(() => {
    const getCreditFormData = async () => {
      const data = await creditFormApi.getDateForCreditForm();
      setSelectData(data);
    };
    getCreditFormData();
  }, []);

  const {
    handleChange,
    values,
    handleSubmit,
    touched,
    errors,
    isValidating,
    setFieldValue,
    isSubmitting,

    handleBlur,
    setFieldTouched,
    validateField,
  } = useFormik({
    initialValues: initialFormData,
    validationSchema: validationSchema,
    validateOnChange: false,
    validateOnBlur: false,
    onSubmit: async values => {
      try {
        await creditFormApi.submitForm(values);
        setSuccessfulSubmit(true);
      } catch (error) {
        alert('Something went wrong');
      }
    },
  });

  const debouncedValidate = useMemo(() => debounce(validateField, 500), []);

  useEffect(() => {
    debouncedValidate('email');
  }, [values.email, debouncedValidate]);

  useEffect(() => {
    validateField('description');
    validateField('reflection');
    validateField('activityType');
    validateField('creditAmount');
    validateField('creditType');
    validateField('dateCompleted');
  }, [
    values.description,
    values.reflection,
    values.activityType,
    values.creditAmount,
    values.creditType,
    values.dateCompleted,
    validateField,
  ]);

  return (
    <div className="flex items-center">
      {!successfulSubmit ? (
        <form
          onSubmit={handleSubmit}
          className="flex flex-col md:flex-row w-full rounded-lg overflow-hidden"
        >
          <div className={cn(formColumnStyles, 'md:py-32 bg-[#fff]')}>
            <h2 className="text-2xl md:text-3xl lg:text-4xl font-extrabold text-center mb-3">
              Submit External Credit
            </h2>
            <p className="text-center mb-3 font-medium">Enter your credit details below</p>
            <InputField
              name="email"
              placeholder="Email"
              checkIcon={true}
              isValidating={isValidating}
              asyncValidating={true}
              onChange={handleChange}
              touched={touched}
              errors={errors}
              value={values.email}
              onBlur={handleBlur}
              setFieldTouched={setFieldTouched}
            />
            <MySelect
              options={selectData.activityType.map(option => ({
                value: option,
                label: option,
              }))}
              name="activityType"
              placeholder="Select an Activity Type"
              setFieldValue={setFieldValue}
              touched={touched}
              errors={errors}
              validateField={validateField}
            />
            <DatePicker
              selected={values.dateCompleted}
              onChange={(date: Date | null) => setFieldValue('dateCompleted', date)}
              maxDate={maxDate}
              className="w-full py-3 px-4 bg-[#eeeeee] placeholder:text-slate-400 placeholder:font-medium"
              placeholderText="Date Completed"
            />
            {touched['dateCompleted'] && errors['dateCompleted'] && (
              <div className="text-red-400">{errors['dateCompleted']}</div>
            )}
          </div>
          <div className={cn(formColumnStyles, 'bg-[#66869b]')}>
            <div className="mb-3">
              <MySelect
                options={selectData.creditType.map(option => ({
                  value: option,
                  label: option,
                }))}
                name="creditType"
                placeholder="Select a Credit Type"
                setFieldValue={setFieldValue}
                touched={touched}
                errors={errors}
                validateField={validateField}
              />
              <MySelect
                options={selectData.creditAmount.map(option => ({
                  value: option,
                  label: option,
                }))}
                name="creditAmount"
                placeholder="Select Credit Amount"
                setFieldValue={setFieldValue}
                touched={touched}
                errors={errors}
                validateField={validateField}
              />
              <InputField
                name="description"
                placeholder="Description"
                value={values.description}
                onChange={handleChange}
                errors={errors}
                touched={touched}
                setFieldTouched={setFieldTouched}
                onBlur={handleBlur}
              />
              <InputField
                name="reflection"
                placeholder="Reflection"
                value={values.reflection}
                onChange={handleChange}
                errors={errors}
                touched={touched}
                setFieldTouched={setFieldTouched}
                onBlur={handleBlur}
              />
            </div>
            <p className="text-[#dae5ea] text-[17px] text-center mb-10">
              Ensure all details are correct before submission
            </p>
            <button
              // disabled={isSubmitting || !isValid}
              disabled={isSubmitting || Boolean(errors['email'])}
              type="submit"
              className="m-auto block bg-[#0090a8] py-3 px-11 rounded-full text-[#dae5ea] font-medium disabled:opacity-40"
            >
              SUBMIT CREDIT
            </button>
          </div>
        </form>
      ) : (
        <SuccessfulSubmit />
      )}
    </div>
  );
};

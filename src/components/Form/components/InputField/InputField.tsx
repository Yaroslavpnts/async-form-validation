import { InputHTMLAttributes } from 'react';
import cn from 'classnames';
import CompleteIcon from './assets/complete.svg?react';
import Spinner from './assets/spinner.gif';

interface IInputFieldProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  value: string;
  styles?: string;
  checkIcon?: boolean;
  isValidating?: boolean;
  asyncValidating?: boolean;
  errors: { [field: string]: string };
  touched: { [field: string]: boolean };
  onChange: React.ChangeEventHandler<HTMLInputElement>;
  onBlur: React.FocusEventHandler<HTMLInputElement>;
  setFieldTouched: (
    field: string,
    touched?: boolean | undefined,
    shouldValidate?: boolean | undefined
  ) => void;
}

export const InputField: React.FC<IInputFieldProps> = ({
  styles,
  checkIcon,
  isValidating,
  asyncValidating,
  touched,
  errors,
  onChange,
  setFieldTouched,
  ...props
}) => {
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFieldTouched(props.name, true, false);
    onChange(e);
  };

  const isShowError = touched[props.name] && errors[props.name];

  return (
    <div className="relative mb-5">
      <input
        className={cn(
          'bg-[#eeeeee] pl-4 pr-9 py-3 placeholder:text-slate-400 placeholder:font-medium w-full focus:outline-0',
          { 'outline outline-1 outline-red-400': isShowError },
          styles
        )}
        {...props}
        onChange={handleChange}
      />
      {isShowError && <div className="text-red-400">{errors[props.name]}</div>}
      {asyncValidating && isValidating && (
        <img src={Spinner} className="absolute right-2 top-6 -translate-y-1/2" />
      )}
      {checkIcon && touched[props.name] && !errors[props.name] && !isValidating && (
        <CompleteIcon className="absolute right-2 top-6 -translate-y-1/2" />
      )}
    </div>
  );
};

import { FC } from 'react';
import Select, { SingleValue } from 'react-select';

type TOption = { value: string; label: string };

interface SelectProps {
  name: string;
  options: TOption[];
  placeholder?: string;
  setFieldValue: (field: string, value: string, shouldValidate?: boolean | undefined) => void;
  errors: { [field: string]: string };
  touched: { [field: string]: boolean };
  validateField: (field: string) => void;
}

export const MySelect: FC<SelectProps> = ({
  options,
  placeholder,
  name,
  setFieldValue,
  errors,
  touched,
  // , value
}) => {
  const onSelect = (newValue: SingleValue<TOption>) => {
    setFieldValue(name, newValue?.value || '');
  };

  const isError = touched[name] && errors[name];

  return (
    <div className="relative mb-5">
      <Select
        options={options}
        placeholder={placeholder}
        isSearchable={false}
        onChange={onSelect}
        styles={{
          control: baseStyles => ({
            ...baseStyles,
            backgroundColor: '#eeeeee',
            padding: '6px',
            border: 'none',
            boxShadow: 'none',
            borderRadius: 'unset',
            outline: isError ? '#F87171 solid 1px' : '',
          }),
          placeholder: baseStyles => ({
            ...baseStyles,
            fontWeight: 'bold',
            color: '#212121',
          }),
          indicatorSeparator: () => ({
            display: 'none',
          }),
        }}
      />
      {touched[name] && errors[name] && <div className="text-red-400">{errors[name]}</div>}
    </div>
  );
};
